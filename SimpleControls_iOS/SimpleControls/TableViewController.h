//
//  TableViewController.h
//  SimpleControl
//
//  Created by Cheong on 7/11/12.
//  Copyright (c) 2012 RedBearLab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BLE.h"

@interface TableViewController : UITableViewController <BLEDelegate>
{
    IBOutlet UIButton *btnConnect;
    IBOutlet UISwitch *swDigitalIn;
    IBOutlet UISwitch *swDigitalOut;
    IBOutlet UISwitch *swAnalogIn;
    IBOutlet UILabel *lblAnalogIn;
    IBOutlet UISlider *sldPWM;
    IBOutlet UISlider *sldBlink;
    IBOutlet UISlider *sldFade;
    IBOutlet UIActivityIndicatorView *indConnecting;
    IBOutlet UILabel *lblRSSI;
    IBOutlet UILabel *lblDigitalOutPin;
    IBOutlet UILabel *lblDigitalInPin;
    IBOutlet UILabel *lblPWMPin;
    IBOutlet UILabel *lblBlinkPin;
    IBOutlet UILabel *lblFadePin;
    IBOutlet UILabel *lblAnalogPin;
    IBOutlet UISwitch *swDigitalStartRead;
    IBOutlet UISwitch *swStartBlink;
    IBOutlet UISwitch *swStartFade;
}

@property (strong, nonatomic) BLE *ble;

@end
