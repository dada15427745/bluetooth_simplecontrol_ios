//
//  TableViewController.m
//  SimpleControl
//
//  Created by Cheong on 7/11/12.
//  Copyright (c) 2012 RedBearLab. All rights reserved.
//

#import "TableViewController.h"
//
@interface TableViewController ()

@end

@implementation TableViewController

@synthesize ble;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    ble = [[BLE alloc] init];
    [ble controlSetup:1];
    ble.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - BLE delegate

- (void)bleDidDisconnect
{
    NSLog(@"->Disconnected");

    [btnConnect setTitle:@"Connect" forState:UIControlStateNormal];
    [indConnecting stopAnimating];
    
    lblAnalogIn.enabled = false;
    swDigitalOut.enabled = false;
    swDigitalIn.enabled = false;
    swAnalogIn.enabled = false;
    sldPWM.enabled = false;
    sldBlink.enabled = false;
    sldFade.enabled = false;
    swDigitalStartRead.enabled = false;
    swStartBlink.enabled = false;
    swStartFade.enabled = false;
    
    lblRSSI.text = @"---";
 //   lblAnalogIn.text = @"----";
}

// When RSSI is changed, this will be called
-(void) bleDidUpdateRSSI:(NSNumber *) rssi
{
    lblRSSI.text = rssi.stringValue;
}

// When disconnected, this will be called
-(void) bleDidConnect
{
    NSLog(@"->Connected");

    [indConnecting stopAnimating];
    
    lblAnalogIn.enabled = true;
    swDigitalOut.enabled = true;
    swDigitalIn.enabled = true;
    swAnalogIn.enabled = true;
    sldPWM.enabled = true;
    swDigitalStartRead.enabled = true;
    swStartBlink.enabled = true;
    swStartFade.enabled = true;
    
    swDigitalOut.on = false;
    swDigitalIn.on = false;
    swAnalogIn.on = false;
    sldPWM.value = 0;
    sldBlink.value = 0;
    swDigitalStartRead.on = false;
    swStartBlink.on = false;
    swStartFade.on = false;
}

// When data is comming, this will be called
-(void) bleDidReceiveData:(unsigned char *)data length:(int)length
{
    NSLog(@"Length: %d", length);

    // parse data, all commands are in 4-byte
    for (int i = 0; i < length; i+=4)
    {
        NSLog(@"0x%02X, 0x%02X, 0x%02X, 0x%02X", data[i], data[i+1], data[i+2], data[i+3]);

        if (data[i] == 0x0A)
        {
            lblDigitalInPin.text = [NSString stringWithFormat:@"<%d>",data[i+1]];
            if (data[i+2] == 0x01)
                swDigitalIn.on = true;
            else
                swDigitalIn.on = false;
        }
        else if (data[i] == 0x0B)
        {
            UInt16 Value;
            
            Value = data[i+3] | data[i+2] << 8;
            lblAnalogPin.text = [NSString stringWithFormat:@"<%d>",data[i+1]];
            lblAnalogIn.text = [NSString stringWithFormat:@"%d", Value];
        }        
    }
}

#pragma mark - Actions

// Connect button will call to this
- (IBAction)btnScanForPeripherals:(id)sender
{
    if (ble.activePeripheral)
        if(ble.activePeripheral.isConnected)
        {
            [[ble CM] cancelPeripheralConnection:[ble activePeripheral]];
            [btnConnect setTitle:@"Connect" forState:UIControlStateNormal];
            return;
        }
    
    if (ble.peripherals)
        ble.peripherals = nil;
    
    [btnConnect setEnabled:false];
    [ble findBLEPeripherals:2];
    
    [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    
    [indConnecting startAnimating];
}

-(void) connectionTimer:(NSTimer *)timer
{
    [btnConnect setEnabled:true];
    [btnConnect setTitle:@"Disconnect" forState:UIControlStateNormal];
    
    if (ble.peripherals.count > 0)
    {
        [ble connectPeripheral:[ble.peripherals objectAtIndex:0]];
    }
    else
    {
        [btnConnect setTitle:@"Connect" forState:UIControlStateNormal];
        [indConnecting stopAnimating];
    }
}

-(IBAction)sendDigitalOut:(id)sender
{
    UInt8 buf[4] = {0x01, 0x02, 0x00, 0x00}; //buf[1] = (1~7) 表示第幾個pin作digital輸出
    
    //buf[1] = 0x01  //指定第x個pin
    lblDigitalOutPin.text = [NSString stringWithFormat:@"<%d>",buf[1]];
    if (swDigitalOut.on)
        buf[2] = 0x01;
    else
        buf[2] = 0x00;
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}

/* Send command to Arduino to enable analog reading */
-(IBAction)sendAnalogIn:(id)sender
{
    UInt8 buf[4] = {0xB0, 0x00, 0x00, 0x00}; //buf[1] = (0~4) 表示第幾個pin作analog輸入
    
    //buf[1] = 0x00
    lblAnalogPin.text = [NSString stringWithFormat:@"<%d>",buf[1]];
    if (swAnalogIn.on)
        buf[2] = 0x01;
    else
        buf[2] = 0x00;
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}

/* Send command to Arduino to enable digital reading */
-(IBAction)sendDigitalIn:(id)sender
{
    UInt8 buf[4] = {0xA0, 0x04, 0x00, 0x00}; //buf[1] = (1~7) 表示第幾個pin作analog輸入
    
    //buf[1] = 0x01
    lblDigitalInPin.text = [NSString stringWithFormat:@"<%d>",buf[1]];
    if (swDigitalStartRead.on)
        buf[2] = 0x01;
    else
        buf[2] = 0x00;
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}

// PWM slide will call this to send its value to Arduino
-(IBAction)sendPWM:(id)sender
{
    UInt8 buf[4] = {0x02, 0x03, 0x00, 0x00}; //buf[1] = (3.5.6) 表示第幾個pin作pwm
    
    //buf[1] = 0x03
    lblPWMPin.text = [NSString stringWithFormat:@"<%d>",buf[1]];
    buf[2] = sldPWM.value;
    buf[3] = (int)sldPWM.value >> 8;
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}

/////
// Servo slider will call this to send its value to Arduino

- (IBAction)sendBlinkValue:(id)sender {
    UInt8 buf[4] = {0x03, 0x07, 0x01, 0x00}; //buf[1] = (3.5.7)
    
    //buf[1] = 0x05
    buf[3] = sldBlink.value;
    //buf[3] = (int)sldBlink.value >> 8;  4bytes的參數結構，不能讓Blink的value大於255，會沒地方裝，所以Arduino那邊自己乘大倍率就好。
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}

-(IBAction)sendStartBlink:(id)sender
{
    UInt8 buf[4] = {0x03, 0x07, 0x00, 0x00}; //buf[1] = (3.5.7) 表示第幾個pin作analog輸入
    
    //buf[1] = 0x01
    lblBlinkPin.text = [NSString stringWithFormat:@"<%d>",buf[1]];
    if (swStartBlink.on) {
        buf[2] = 0x01;
        buf[3] = 50;
        sldBlink.enabled = true;
    }
    else {
        buf[2] = 0x00;
        sldBlink.enabled = false;
    }
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}
- (IBAction)sendStartFade:(id)sender {
    UInt8 buf[4] = {0x04, 0x05, 0x00, 0x00}; //buf[1] = (3.5.7) 表示第幾個pin作analog輸入
    
    //buf[1] = 0x01
    lblFadePin.text = [NSString stringWithFormat:@"<%d>",buf[1]];
    if (swStartFade.on) {
        buf[2] = 0x01;
        buf[3] = 50;
        sldFade.enabled = true;
    }
    else {
        buf[2] = 0x00;
        sldFade.enabled = false;
    }
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}
- (IBAction)sendFadeValue:(id)sender {
    UInt8 buf[4] = {0x04, 0x05, 0x01, 0x00}; //buf[1] = (3.5.7)
    
    buf[3] = sldFade.value;
    //    buf[3] = (int)sldServo.value >> 8;
    
    NSData *data = [[NSData alloc] initWithBytes:buf length:4];
    [ble write:data];
}

@end
